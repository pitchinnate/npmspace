package main

import (
	"flag"
	"fmt"
	tm "github.com/buger/goterm"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"
)

type nodeDir struct {
	Directory string
	Size int64
}

func main() {
	var verbose = flag.Bool("v", false, "enable verbose logging")
	var directory = flag.String("d", "/", "run on specific directory")
	flag.Parse()

	nodeDirectories := []nodeDir{}
	inNodeDir := make(chan nodeDir)
	jobSChan := make(chan int)
	jobCChan := make(chan int)
	totalDir := make(chan int)

	jobsStarted := 1
	jobsComplete := 0
	directoriesSearched := 0

	go ReadDir(*directory, inNodeDir, jobSChan, jobCChan, totalDir)
	ticker := time.NewTicker(100 * time.Millisecond)

	if !*verbose {
		tm.Print("\n\n\n\n\n")
	}

	for {
		select {
		case <-ticker.C:
			if !*verbose {
				DisplayStats(nodeDirectories, directoriesSearched, *verbose)
			}
		case <- totalDir:
			directoriesSearched += 1
		case newNodeDir := <-inNodeDir:
			nodeDirectories = append(nodeDirectories, newNodeDir)
			if *verbose {
				fmt.Printf("%s %d\n", newNodeDir.Directory, newNodeDir.Size)
			}
		case newJob := <- jobSChan:
			jobsStarted += newJob
		case completeJob := <- jobCChan:
			jobsComplete += completeJob
		}
		if jobsComplete == jobsStarted {
			break
		}
	}
	DisplayStats(nodeDirectories, directoriesSearched, *verbose)
}

func DisplayStats(nodeDirectories []nodeDir, directoriesSearched int, verbose bool) {
	var totalSize int64
	totalSize = 0
	for _, dir := range nodeDirectories {
		totalSize += dir.Size
	}
	//tm.MoveCursor(1,1)
	if !verbose {
		tm.MoveCursorUp(5)
	}
	tm.Print("----------------------------------------\n")
	tm.Printf("Total directories searched: %d\n", directoriesSearched)
	tm.Print("----------------------------------------\n")
	tm.Print(tm.Color("node_modules", tm.RED))
	tm.Printf(" directories found: %d\n", len(nodeDirectories))
	tm.Printf("Total Space Used: %s           \n", ByteCountIEC(totalSize))
	tm.Flush()
}

func ReadDir(directory string, nodeChannel chan nodeDir, jobSChan chan int, jobCChan chan int, totalDir chan int) {
	//log.Printf("job started: %s", directory)
	totalDir <- 1
	files, err := ioutil.ReadDir(directory)
	if err != nil {
		//log.Printf("Error reading dir %s", err)
	} else {
		for _, file := range files {
			if file.IsDir() {
				//log.Printf("Found Dir: %s", file.Name())
				path := filepath.Join(directory, file.Name())
				if file.Name() == "node_modules" {
					size, _ := DirSize(path)
					nodeChannel <- nodeDir{path, size}
				} else {
					jobSChan <- 1
					go ReadDir(path, nodeChannel, jobSChan, jobCChan, totalDir)
				}
			}
		}
	}
	jobCChan <- 1
}

func DirSize(path string) (int64, error) {
	var size int64
	err := filepath.Walk(path, func(_ string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			size += info.Size()
		}
		return err
	})
	return size, err
}

func ByteCountIEC(b int64) string {
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%d B", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f %ciB",
		float64(b)/float64(div), "KMGTPE"[exp])
}
