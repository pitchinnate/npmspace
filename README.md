Simple app built in Go to find all `node_modules` directories on your computer and report back how many it found and how much drive space they are taking up.

```shell
npmspace -h
Usage of npmSpace:
  -d string
        run on specific directory (default "/")
  -v    enable verbose logging

# defaults to search entire drive
npmspace

# can use on a single directory (will still walk down to children directories)
npmspace -d /var/www

# enable verbose logging to see route to each directory found
npmspace -v
```
